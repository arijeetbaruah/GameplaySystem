// Copyright Epic Games, Inc. All Rights Reserved.

#include "CombatRPGGameMode.h"
#include "CombatRPGCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACombatRPGGameMode::ACombatRPGGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
