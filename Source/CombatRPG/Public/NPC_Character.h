// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GDAbilityInputID.h"
#include "NPC_Character.generated.h"

class UAbilitySystemComponent;
class UCharacterHealthAttributeSet;
class UGameplayAbility;
struct FOnAttributeChangeData;

UCLASS()
class COMBATRPG_API ANPC_Character : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANPC_Character();

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	UAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	const UCharacterHealthAttributeSet* HPAttributeSet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	TMap<EGDAbilityInputID, TSubclassOf<UGameplayAbility>> Abilities;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Inherited via IAbilitySystemInterface
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	void OnHealthChangedInternal(const FOnAttributeChangeData& value);
};
