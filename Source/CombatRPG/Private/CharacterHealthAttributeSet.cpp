// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthAttributeSet.h"
#include "GameplayPrediction.h"
#include "AttributeSet.h"
#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"

void UCharacterHealthAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(GetHealth() + Data.EvaluatedData.Magnitude);
	}
}

void UCharacterHealthAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UCharacterHealthAttributeSet, Health, OldHealth);
}

void UCharacterHealthAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UCharacterHealthAttributeSet, Health, COND_None, REPNOTIFY_Always);
}
