// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC_Character.h"
#include "AbilitySystemComponent.h"
#include "GameplayEffectTypes.h"
#include "Abilities/GameplayAbility.h"
#include "GameplayAbilitySpec.h"
#include "GDAbilityInputID.h"
#include "CharacterHealthAttributeSet.h"

// Sets default values
ANPC_Character::ANPC_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystem"));
}

// Called when the game starts or when spawned
void ANPC_Character::BeginPlay()
{
	Super::BeginPlay();

	HPAttributeSet = AbilitySystemComponent->GetSet<UCharacterHealthAttributeSet>();
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(HPAttributeSet->GetHealthAttribute()).AddUObject(this, &ANPC_Character::OnHealthChangedInternal);
	
	for (const TPair<EGDAbilityInputID, TSubclassOf<UGameplayAbility>> pair : Abilities)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(pair.Value, 1, static_cast<uint32>(pair.Key)));
	}
}

void ANPC_Character::OnHealthChangedInternal(const FOnAttributeChangeData& value)
{
}

// Called every frame
void ANPC_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ANPC_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ANPC_Character::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

