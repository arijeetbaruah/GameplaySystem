// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CombatRPGGameMode.generated.h"

UCLASS(minimalapi)
class ACombatRPGGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACombatRPGGameMode();
};



